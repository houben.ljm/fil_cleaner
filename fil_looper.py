#!/usr/bin/env python

"""
fil_looper.py

Reads a Filterbank file BLOCK by BLOCK and feeds the loaded chunk of
data into a given function for processing.

Leon Houben
"""

import os,sys
import datetime
import time
import argparse
import numpy as np
import logging
import set_logging
import queue
import matplotlib.pyplot as plt
import multiprocessing as mp
from multiprocessing.managers import SyncManager
from queue import PriorityQueue
from presto import filterbank

logger = logging.getLogger(__name__)

"""
Register the Priority Queue to the multiprocessing manager,
so we can use it instead of the normal Queue.
"""
class MyManager(SyncManager):
    pass

MyManager.register("PriorityQueue", PriorityQueue)

##############################################################################
# Filterbank processing class
##############################################################################
class looper(object):
    def __init__(self,infile,outname=False,BLOCK=2**13,ncores=False,verbose=False):
        """Floop constructor

            Input:
                infile:  Path to Filterbank file that needs to be looped through.
                outname: Name of new output Filterbank file.
                BLOCK:   Number of spectra to read in at once.
                verbose: Wether to print more processing info to screen.

            Output:
                Floop object
        """
        self.infile  = infile
        self.outname = outname
        self.BLOCK   = BLOCK
        self.nBLOCK  = 0
        if ncores is None:
            self.ncores = mp.cpu_count()
            logger.info(f"\nMultiprocessing using {self.ncores} cores.")
        else:
            self.ncores  = ncores
        self.qlim    = 4e9 #bytes
        self.verbose = verbose

        ### Load Filterbank file
        if infile.endswith(".fil"):
            # Filterbank file
            if not os.path.isfile(infile):
                log_str = "%s does not exist!" % infile
                logger.error(log_str)
                raise IOError(log_str)
            self.rfil        = filterbank.FilterbankFile(infile)
        else:
            log_str = "Cannot recognize data type from file extension. (Only Filterbank data is supported!)"
            logger.error(log_str)
            raise ValueError(log_str)

        ### Possibly create new Filterbank file
        if outname != False:
            self.new_fil(outname)

    def new_fil(self,outname=None,nbits=None,tsamp=None):
        if outname is None:
            now = datetime.datetime.now()
            outname = 'new_'+self.infile.split('.fil')[0]+now.strftime("_%d-%m-%Y_%H:%M:%S")+'.fil'
        if nbits is None:
            nbits = self.rfil.nbits

        new_header = self.rfil.header.copy()
        if tsamp is None:
            new_header['tsamp'] = self.rfil.tsamp

        logger.info('\nCreating file: %s ' % outname)
        self.wfil = filterbank.create_filterbank_file(outname, new_header, nbits=nbits, mode='append')

        return

    def start_loop(self,process_func,start_nspec=0,**proc_func_kwargs):
        start_time_loop = time.time()
        if self.ncores:
            logger.info("\nPerforming an initialisation run...")
            single_proc_time = time.time()
            self.singleprocess_loop(process_func,start_nspec=start_nspec,initialisation_run=True,**proc_func_kwargs)
            single_proc_time = time.time() - single_proc_time

            if single_proc_time <= 1.5:
                logger.warning(f"\nThe initialisation run only took {single_proc_time} seconds!!!")
                logger.warning("Multi-processing the remainder of the data won't gain you in speed.")
                logger.warning("Reverting back to using a single core.")
                self.singleprocess_loop(process_func,start_nspec=(start_nspec+self.BLOCK),**proc_func_kwargs)
            else:
                logger.info("\nMulti-processing remainder of data...")
                self.multiprocess_loop(process_func,start_nspec=(start_nspec+self.BLOCK),ncores=self.ncores,**proc_func_kwargs)
        else:
            self.singleprocess_loop(process_func,start_nspec=start_nspec,**proc_func_kwargs)

        end_time_loop = time.time()
        logger.info(f"\nThat took {(end_time_loop - start_time_loop)} seconds")


    def singleprocess_loop(self,process_func,start_nspec=0,initialisation_run=False,**proc_func_kwargs):
        #Give info on progress
        log_str = "\nProcessing %s... " % self.infile
        logger.info(log_str)
        oldprogress = 0
        if not self.verbose:
            print(log_str)
            sys.stdout.write(" %3.0f %%\r" % oldprogress)
            sys.stdout.flush()

        spec_start = start_nspec
        spec_end   = start_nspec+self.BLOCK
        while True:
            # Get spectra instance
            spectra = self.rfil.get_spectra(spec_start, (spec_end-spec_start)).data

            # Print progress to screen (1/2)
            progress = int(100.0*spec_end/self.rfil.nspec)
            # Give info on progress
            logger.info("\nProcessing spectra %i to %i; %3.0f%% of total. "% (spec_start,spec_end,progress))

            # Do something with spectra
            spectra = process_func(spectra,self.nBLOCK,nspec=spec_start,tsamp=self.rfil.tsamp, \
                                   fch1=self.rfil.fch1,foff=self.rfil.foff,**proc_func_kwargs)

            if hasattr(self,'wfil'):
                # Write spectra to new file
                self.wfil.append_spectra(spectra.transpose())
                logger.info(f"    Spectra from block {self.nBLOCK} written to Filterbank file")

            # Print progress to screen (2/2)
            if not self.verbose and (progress > oldprogress):
                sys.stdout.write(" %3.0f %%\r" % progress)
                sys.stdout.flush()
                oldprogress = progress

            # Prepare for next iteration
            spec_start  += self.BLOCK
            spec_end     = min(spec_start+self.BLOCK, self.rfil.nspec)
            self.nBLOCK += 1

            # Initialisation run or reached end of file?
            if initialisation_run:
                break
            elif spec_start > self.rfil.nspec:
                break

        if not initialisation_run:
            self.rfil.close()
            if hasattr(self,'wfil'):
                self.wfil.close()
            sys.stdout.write("Done\n\n")
            sys.stdout.flush()

        return

    def process_data(self,queue,queue_size,qs_lock,process_func,spectra,nBLOCK,**proc_kwargs):
        # Give info on progress
        logger.info(f"\nProcessing block {nBLOCK} in process {os.getpid()}")
        logger.info(f"    I.e. spectra {proc_kwargs['nspec']} to {min(proc_kwargs['nspec']+self.BLOCK, self.rfil.nspec)}")
        # Do something with spectra
        spectra = process_func(spectra,nBLOCK,**proc_kwargs)
        spectra_size = sys.getsizeof(spectra)
        # Put result into queue
        puttime = time.time()
        queue.put((nBLOCK,spectra,spectra_size))
        qs_lock.acquire()
        queue_size.value += spectra_size
        qs_lock.release()
        logger.info(f"        Done; block {nBLOCK} was put in queue in {time.time()-puttime} seconds")

    def consume_queue(self,queue,queue_size,fill_q,procs_finished,w2f):
        """
        Consume all queue entries.
        If w2f is set, write queue entries to Filterbank file.
        """
        terminate = False
        iterval   = 1
        while True:
            retrievetime = time.time()
            num,data,data_size = queue.get()
            queue_size.value -= data_size
            logger.info(f"        Retrieved block {num} from queue in {time.time() - retrievetime} seconds")

            if num == iterval:
                iterval += 1
                if w2f.is_set():
                    # Write spectra to new file
                    self.wfil.append_spectra(data.transpose())
                    logger.info(f"    Block {num} written to Filterbank file.")
            else:
                logger.info(f"            Puting block {num} back into queue")
                queue.put((num,data,data_size))
                queue_size.value += data_size

            queue.task_done()

            if queue.empty():
                if procs_finished.is_set():
                    logger.info("\nQueue fully emptied!")
                    break
                else:
                    fill_q.set()

    def multiprocess_loop(self,process_func,start_nspec=0,ncores=4,**proc_func_kwargs):
        #Give info on progress
        log_str = "\nProcessing %s... " % self.infile
        logger.info(log_str)
        oldprogress = 0
        if not self.verbose:
            print(log_str)
            sys.stdout.write(" %3.0f %%\r" % oldprogress)
            sys.stdout.flush()

        # Set initial loop values
        spec_start = start_nspec
        spec_end   = start_nspec+self.BLOCK
        terminate_loop = False
        with MyManager() as manager:
            # Set-up the queue to process data products
            procs_finished = manager.Event()
            w2f            = manager.Event()
            fill_q         = manager.Event()
            fill_q.set()
            in_queue       = manager.Value('i', 0)
            out_queue      = manager.Value('i', 0)
            inq_lock       = manager.Lock()
            queue          = manager.PriorityQueue()
            if hasattr(self,'wfil'):
                # Write data products to new fil file
                w2f.set()

            proc_queue     = mp.Process(target=self.consume_queue, args=(queue,out_queue,fill_q,procs_finished,w2f))
            proc_queue.start()

            while True:
                procs  = []
                for _ in range(ncores):
                    # Wait when size of queue is too large
                    qsize = in_queue.value + out_queue.value
                    if qsize >= self.qlim:
                        logger.warning(f"\nWARNING: queue size exceeds the set limit of {self.qlim} bytes. Emptying queue...")
                        fill_q.clear()
                        fill_q.wait()

                    # Get spectra instance
                    spectra = self.rfil.get_spectra(spec_start, (spec_end-spec_start)).data

                    kwargs_topass = {'nspec':spec_start, 'tsamp':self.rfil.tsamp, \
                                     'fch1':self.rfil.fch1, 'foff':self.rfil.foff}
                    kwargs_topass.update(proc_func_kwargs)
                    proc = mp.Process(target=self.process_data, args=(queue,in_queue,inq_lock,process_func,spectra,self.nBLOCK), \
                                      kwargs=(kwargs_topass))
                    procs.append(proc)
                    proc.start()

                    # Prepare for next process
                    spec_start  += self.BLOCK
                    spec_end     = min(spec_start+self.BLOCK, self.rfil.nspec)
                    self.nBLOCK += 1

                    # Reached end of file?
                    if spec_start > self.rfil.nspec:
                        terminate_loop = True
                        break

                # Join processes
                for proc in procs:
                    proc.join()

                # Print progress to screen
                progress = int(100.0*spec_end/self.rfil.nspec)
                if not self.verbose and (progress > oldprogress):
                    sys.stdout.write(" %3.0f %%\r" % progress)
                    sys.stdout.flush()
                    oldprogress = progress
                
                if terminate_loop:
                    procs_finished.set()
                    proc_queue.join()
                    break

        self.rfil.close()
        if hasattr(self,'wfil'):
            self.wfil.close()
        sys.stdout.write("Done\n\n")
        sys.stdout.flush()

        return


def downsamp(spectra, nBLOCK, fraction=2, **kwargs):
    mod = (spectra.shape[1] % fraction)
    if mod != 0:
        # Append spectra with median values to enable rebinning
        med  = np.median(spectra,axis=1)
        ones = np.ones((spectra.shape[0],(spectra.shape[1]//fraction + 1)*fraction-spectra.shape[1]))
        logger.warning(f"\nData were appended with {ones.shape[1]} additional spectra to allow downconversion.")
        spectra = np.append(spectra,ones*med[:,None],axis=1)

    return rebin2D(spectra, fraction)

def rebin2D(arr, binf, chanf=1):
    logger.info(f"    Downsampling with a factor of {binf} in time.")
    sh = arr.shape[0]//chanf,chanf,arr.shape[1]//binf,binf
    return arr.reshape(sh).mean(-1).mean(1)

def main():
    Floop = looper(args.infile,False,args.BLOCK,args.ncores,args.verbose)
    Floop.new_fil(outname=args.outname,tsamp=args.frac*Floop.rfil.tsamp)
    Floop.start_loop(downsamp,fraction=args.frac)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(prog='fil_looper.py', formatter_class=argparse.ArgumentDefaultsHelpFormatter, \
                                     description="Helper class to loop over fil files.\nNativally downsamples data in time.")

    parser.add_argument("infile", \
                            help="Filterbank file to process.")
    parser.add_argument("--frac", type=int, default=2, \
                            help="Fraction with which to downsample the data.")
    parser.add_argument("--block-size", dest='BLOCK', type=int, default=2**15, \
                            help="Number of spectra per read. This is the amount of data processed at a time.")
    parser.add_argument("-j", "--ncores", nargs='?', type=int, default=False,
                            help="Number of cores to use for processing.")
    parser.add_argument("-o", "--outname", dest='outname', default=None,
                            help="Name of output Filterbank file.")
    parser.add_argument("-v", "--verbose", action='store_true', default=False, \
                            help="Print more operation details")

    args = parser.parse_args()
    if not ((args.frac & (args.frac-1) == 0) and args.frac != 0):
        parser.error("The given fraction must be a power of 2!")
    elif (args.BLOCK % args.frac) != 0:
        parser.error("The chosen BLOCK size must be devidable by the given fraction!")

    #Configure logger
    set_logging.main(args.infile.split('.fil')[0],args.verbose)

    main()
